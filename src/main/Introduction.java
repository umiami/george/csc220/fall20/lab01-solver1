public class Introduction {

  /**
   * Simulate coin flipping resulting into either heads
   * or tails with a 55% chance in favor of heads.
   *
   * @return   string heads or tails
   */
  public static String coinFlip() {
    if (Math.random() < 0.55) {
      return "heads";
    }
    return "tails";
  }

  /**
   * Find the smallest value in given array that sums up with
   * another element from the array into 20.
   *
   * @example  {5, 7, 8, 9, 10, 15, 16} -> 0
   * @example  {3, 5, 8, 9, 10, 15, 16} -> 1
   * @example  {3, 4, 6, 9, 10, 14, 15} -> 2
   * @example  {6, 7, 8, 9, 10, 15, 16} -> -1
   *
   * @param    input array of ascending integers
   * @return   int position of the lower value, -1 if not found
   */
  public static int checkSum(int[] input) {
    int i = 0;
    int j = input.length - 1;
    while (i < j) {
      if (input[i] + input[j] == 20) {
        return i;
      }
      if (input[i] + input[j] > 20) {
        j--;
        continue;
      }
      i++;
    }
    return -1;
  }

  /**
   * The main method generates following string outputs:
   * - Print a simple "Hello world!" string.
   * - Run coinFlip method 100 times and print how many times heads occurred.
   *   Expected result is a number around 55 give or take.
   * - Print checkSum method result with 4 arrays as indicated.
   *   Expected results are 0, 1, 2, -1.
   */
  public static void main(String[] args) {
    System.out.println("Hello world!");
    int heads = 0;
    for (int i = 0; i < 100; i++) {
      if (coinFlip() == "heads") {
        heads++;
      }
    }
    System.out.println(heads);
    System.out.println(checkSum(new int[]{5, 7, 8, 9, 10, 15, 16}));
    System.out.println(checkSum(new int[]{3, 5, 8, 9, 10, 15, 16}));
    System.out.println(checkSum(new int[]{3, 4, 6, 9, 10, 14, 15}));
    System.out.println(checkSum(new int[]{6, 7, 8, 9, 10, 15, 16}));
  }

}
